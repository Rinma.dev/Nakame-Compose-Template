local nk = require("nakama")
nk.logger_info("Lua example module loaded.")

local function healtcheck_rpc(context, payload)
    nk.logger_info("Healthcheck RPC called")
    return nk.json_encode({ ["success"] = true })
end

nk.register_rpc(healtcheck_rpc, "healtcheck_lua")