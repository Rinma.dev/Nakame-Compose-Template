# Nakame-Compose-Template

## Description
### Prerequisite
* docker
* docker-compose
* a running [Traefik container with docker provider](https://doc.traefik.io/traefik/getting-started/quick-start/)

### Start
To run a [Nakama](https://heroiclabs.com/docs/nakama-download/) instance:
1. Update `.env` to the correct values
    * `name`: The name of the instance and part of the subdomain
    * `domain`: The top-level domain you want to use
2. Run `setup.sh` to generate the passwords and keys and start the container

After the container is started you can loginm to the web console. You will get an url like this: `https://gameserver-${name}.${domain}` with default values--> `https://gameserver-nakama.local.host`

You find the username and password for the initial login under `nakama/custom-config.yml` on the local file system or `/nakama/data/custom-config.yml` in the container

### Traefik
In the traefik.toml, change the email in the certificatesResolvers section.
You only need one traefik instance per (physical) server.