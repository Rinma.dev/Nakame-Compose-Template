#!/usr/bin/bash

ADMIN_PASSWORD="$(openssl rand -base64 48)"
SOCKET_KEY="$(openssl rand -base64 48)"
SESSION_KEY="$(openssl rand -base64 48)"
RUNTIME_KEY="$(openssl rand -base64 48)"

FILE="./nakama/custom-config.yml"

sed -i "s@<admin_password>@$ADMIN_PASSWORD@" $FILE
sed -i "s@<socket_server_key>@$SOCKET_KEY@" $FILE
sed -i "s@<session_encryption_key>@$SESSION_KEY@" $FILE
sed -i "s@<runtime_http_key>@$RUNTIME_KEY@" $FILE

docker-compose up -d